#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mymalloc.h"
#include "mytap.h"

static struct test {
    int member1;
    long member2;
    short member3;
} *init_foo(void *ptr)
{
    struct test *foo = ptr;
    foo->member1 = 42;
    foo->member2 = 123456789;
    foo->member3 = 555;
    return foo;
}

int main(int argc, char *argv[])
{
    struct test *foo = init_foo(my_bestfit_malloc(sizeof(struct test)));

    char str[] = "The Hilbertian Entscheidungsproblem can have no solution.";
    char *bar = my_bestfit_malloc(sizeof(str));
    memcpy(bar, str, sizeof(str));

    ok2(
           foo->member1 == 42
        && foo->member2 == 123456789
        && foo->member3 == 555,
        "Ensure foo wasn't corrupted"
    );

    my_free(NULL);
    ok2(1, "Ensure my_free(NULL) is a no-op");

    my_free(foo);
    ok2(1, "Free foo without segfaulting");
    ok2(!strcmp(bar, str), "Verify C<bar> integrity");

    foo = init_foo(my_bestfit_malloc(sizeof(struct test)));
    ok2(!strcmp(bar, str), "Verify C<bar> integrity after reallocing foo");

    my_free(bar);
    ok2(1, "Free C<bar> without segfaulting");
    my_free(foo);
    ok2(1, "Free C<foo> without segfaulting");
    bar = my_bestfit_malloc(sizeof(str));
    memcpy(bar, str, sizeof(str));
    ok2(1, "Test restarting chain from scratch");
    my_free(bar);
    ok2(1, "Test freeing last node");
    return EXIT_SUCCESS;
}
