#ifndef MY_MALLOC
#define MY_MALLOC
void *my_bestfit_malloc(size_t size);
void my_free(void *mem);
#endif
