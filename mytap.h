#define ok1(cond) _ok1((_Bool)(cond), __FILE__, __LINE__)
#define ok2(cond, msg) _ok2((_Bool)(cond), (msg), __FILE__, __LINE__)

void done_testing(void);
int diag(char const *fmt, ...);

void _ok1(_Bool cond, char const *file, unsigned int line);
void _ok2(_Bool cond, char *msg, char const *file, unsigned int line);
