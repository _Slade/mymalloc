# Files #
* mymalloc.c, mymalloc.h, Makefile: Self explanatory.
* mytap.c, mytap.h: Used for the first test program. Their innards are
unimportant.
* test.c: A simple test program that performs a few allocations and
deallocations, checking for memory corruption or leaks.
* test2.c: A second test program. Constructs, traverses, and frees a linked list.

# Building #
The default make target will build all three executables with optimizations
enabled. `make debug` will build all three executables with assertions and
additional debugging output enabled.

After building, `make test` will run the test files. `mallocdrv`'s output
will be truncated to just the important bits. Note that the tests will pause
while the second program waits for input. Simply input some numbers (one per
line) and enter -1 when finished.

`make clean` will remove all object files and executables created during
the build.

# mymalloc.c

A custom memory allocator using a best-fit approach.

## void \*my\_bestfit\_malloc(size\_t size)

Allocate a region capable of fitting `size` bytes and return a pointer to the
base of this region. If size is 0 or an error occurs, `NULL` is returned.

## void my\_free(void \*ptr)

Marks a region previously returned by `my_bestfit_malloc` as ready for reuse.
If `ptr` is `NULL`, this does nothing.
If `ptr` does not point to a valid allocated object, the behavior is undefined.

# Implementation Details

## void \*my\_bestfit\_malloc(size\_t size)

Traverse allocation chain and find the best fit, creating new nodes as necessary
to track each allocation.

In this scheme, successive allocations create what is essentially a linked list
stored on the heap. For example,

    void *a = my_bestfit_malloc(1);
    void *b = my_bestfit_malloc(2);
    void *c = my_bestfit_malloc(3);

would result in this heap:

    +-----------+ <- brk
    |XXXXXXXXXXX| } Allocated region of size 3
    +-----------+ <- Returned address, stored in void *c
    |  node 3   | } Node tracking the above region
    +-----------+
    |XXXXXXXXXXX| } Allocated region of size 2
    +-----------+ <- Returned address, stored in void *b
    |  node 2   | } Node tracking the above region
    +-----------+
    |XXXXXXXXXXX| } Allocated region of size 1
    +-----------+ <- Returned address, stored in void *a
    |  node 1   | } Node tracking the above region
    +-----------+ <- _end

Freeing an allocation node is done by simply updating the adjacent nodes to skip
past it. Following our previous example,

    my_free(b);

would result in this heap:

    +-----------+ <- brk
    |XXXXXXXXXXX| } Allocated region of size 3
    +-----------+ <- Returned address, stored in void *c
    |  node 3   | } Node tracking the above region
    +-----------+
    |???????????| } Free space from where node 2 used to be
    +-----------+
    |XXXXXXXXXXX| } Allocated region of size 1
    +-----------+ <- Returned address, stored in void *a
    |  node 1   | } Node tracking the above region
    +-----------+ <- _end

The free space here would be of size sizeof(Node) plus the size of the allocated
space node 2 was tracking.

This scheme presents a benefit where nodes do not need to be coalesced after a
free because only used space is tracked. Because space can only either be free
or not free, we can assume any space without an allocation node is free space
that can be used for another allocation.

### Region calculation

    +-----------+ <- new region start == sbrk(0)
    |X alloc'd X|
    |X region XX|
    +-----------+ <- _end + sizeof(Node)
    |   head    |
    +-----------+ <- _end, (Node *)g_head_ptr

If there is just one node at the base of the heap, no regions will exist that
can accomodate the request. In this case, the brk is adjusted to make room for a
new allocation node and the requested amount of storage space. If there does not
exist a region s.t. its size is greater than or equal to the request, `size`
will be 0 and `start` will be the current `brk`.

Calculation of space between allocation nodes is done thus:

    :   ...    :
    +----------+
    |node->next|
    +----------+ <- address of node->next
    |??????????| }  reg_size = node->next - free_ptr
    +----------+ <- free_ptr = used_ptr + node->size
    |XXXXXXXXXX| }  allocated space belonging to node
    +----------+ <- used_ptr = node_ptr + sizeof(node)
    |   node   |
    +----------+ <- node_ptr

Where `node_ptr` is the current node of this iteration, `used_ptr` is the
beginning of `node_ptr`'s allocated space, `free_ptr` is the beginning of the
free space _after_ `node_ptr`'s used space, and `reg_size` is the size of
this region of free space.

### Weaknesses and Assumptions

Because of how free locations are discovered, anything else that messes with
this process's heap is liable to have its allocations stomped on, or to break
this malloc. As far as I can tell, this doesn't seem to be a problem as far
as the project's specification, and this allocator is no more susceptible to
problems arising from memory corruption than any other.

This allocator assumes that general pointer comparison is meaningful. To the
extent of my knowledge, this means that it must be running on a system where
virtual address spaces are provided, and that the heap can be treated as if
it were an array (i.e. where indicies to successive elements are given by a
monotonically increasing, linear series of pointer offsets from the address of
the first element).

The underlying hardware must not be too picky about alignment. It should work on
x86 machines (32- and 64-bit).

`my_bestfit_malloc` doesn't use a circular linked list, which means repeated
allocation of similarly sized amounts will likely result in quadratic behavior.
I may change this, but not until I've identified it as an actual bottleneck.

## static void \*new\_node(void \*base, Node \*prev, Node \*next, size\_t size)

Constructs a new Node at `base`.