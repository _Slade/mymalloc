/*
=head1 mymalloc.c

A custom memory allocator using a best-fit approach.

=head2 void *my_bestfit_malloc(size_t size)

Allocate a region capable of fitting C<size> bytes and return a pointer to the
base of this region. If size is 0 or an error occurs, C<NULL> is returned.

=head2 void my_free(void *ptr)

Marks a region previously returned by C<my_bestfit_malloc> as ready for reuse.
If C<ptr> is C<NULL>, this does nothing.
If C<ptr> does not point to a valid allocated object, the behavior is undefined.

=head1 Implementation Details

=cut
*/
#include <assert.h>  /* assert()          */
#include <errno.h>   /* errno             */
#include <signal.h>  /* sigaction()       */
#include <stdbool.h> /* bool, true, false */
#include <stddef.h>  /* NULL, size_t      */
#include <stdint.h>  /* SIZE_MAX          */
#include <stdio.h>   /* perror()          */
#include <stdlib.h>  /* exit()            */
#include <string.h>  /* strerror()        */
#include <unistd.h>  /* sbrk(), intptr_t  */

/* Prototypes, definitions */
typedef struct node {
    struct node *prev;
    struct node *next;
    size_t size; /* Length of contiguous used space following this node, in
                    bytes */
} Node;

void *my_bestfit_malloc(size_t);
void my_free(void *);
static void brk_chk(void *);
static void init_malloc(void);
static void *new_node(void *, Node *, Node *, size_t);
static void *sbrk_chk(intptr_t);
#ifdef DEBUG
static void dump_alloc_chain(void);
static void malloc_debug_exit(void);
#endif

/* Static data */
/* Globals are prefixed with g_ */
static unsigned int g_alloc_count;
static unsigned int g_living_objects; /* This should always be
                                       the number of nodes */
static Node *g_head_ptr;
static Node *g_tail_ptr;
static void *g_end;
static bool g_initialized;

/* Functions */
/**
=head2 void *my_bestfit_malloc(size_t size)

Traverse allocation chain and find the best fit, creating new nodes as necessary
to track each allocation.

In this scheme, successive allocations create what is essentially a linked list
stored on the heap. For example,

    void *a = my_bestfit_malloc(1);
    void *b = my_bestfit_malloc(2);
    void *c = my_bestfit_malloc(3);

would result in this heap:

    +-----------+ <- brk
    |XXXXXXXXXXX| } Allocated region of size 3
    +-----------+ <- Returned address, stored in void *c
    |  node 3   | } Node tracking the above region
    +-----------+
    |XXXXXXXXXXX| } Allocated region of size 2
    +-----------+ <- Returned address, stored in void *b
    |  node 2   | } Node tracking the above region
    +-----------+
    |XXXXXXXXXXX| } Allocated region of size 1
    +-----------+ <- Returned address, stored in void *a
    |  node 1   | } Node tracking the above region
    +-----------+ <- _end

Freeing an allocation node is done by simply updating the adjacent nodes to skip
past it. Following our previous example,

    my_free(b);

would result in this heap:

    +-----------+ <- brk
    |XXXXXXXXXXX| } Allocated region of size 3
    +-----------+ <- Returned address, stored in void *c
    |  node 3   | } Node tracking the above region
    +-----------+
    |???????????| } Free space from where node 2 used to be
    +-----------+
    |XXXXXXXXXXX| } Allocated region of size 1
    +-----------+ <- Returned address, stored in void *a
    |  node 1   | } Node tracking the above region
    +-----------+ <- _end

The free space here would be of size sizeof(Node) plus the size of the allocated
space node 2 was tracking.

This scheme presents a benefit where nodes do not need to be coalesced after a
free because only used space is tracked. Because space can only either be free
or not free, we can assume any space without an allocation node is free space
that can be used for another allocation.

=head3 Region calculation

    +-----------+ <- new region start == sbrk(0)
    |X alloc'd X|
    |X region XX|
    +-----------+ <- _end + sizeof(Node)
    |   head    |
    +-----------+ <- _end, (Node *)g_head_ptr

If there is just one node at the base of the heap, no regions will exist that
can accomodate the request. In this case, the brk is adjusted to make room for a
new allocation node and the requested amount of storage space. If there does not
exist a region s.t. its size is greater than or equal to the request, C<size>
will be 0 and C<start> will be the current C<brk>.

Calculation of space between allocation nodes is done thusly:

    :   ...    :
    +----------+
    |node->next|
    +----------+ <- address of node->next
    |??????????| }  reg_size = node->next - free_ptr
    +----------+ <- free_ptr = used_ptr + node->size
    |XXXXXXXXXX| }  allocated space belonging to node
    +----------+ <- used_ptr = node_ptr + sizeof(node)
    |   node   |
    +----------+ <- node_ptr

Where C<node_ptr> is the current node of this iteration, C<used_ptr> is the
beginning of C<node_ptr>'s allocated space, C<free_ptr> is the beginning of the
free space I<after> C<node_ptr>'s used space, and C<reg_size> is the size of
this region of free space.

=head3 Weaknesses and Assumptions

Because of how free locations are discovered, anything else that messes with
this process's heap is liable to have its allocations stomped on, or to break
this malloc. As far as I can tell, this doesn't seem to be a problem as far
as the project's specification, and this allocator is no more susceptible to
problems arising from memory corruption than any other.

This allocator assumes that general pointer comparison is meaningful. To the
extent of my knowledge, this means that it must be running on a system where
virtual address spaces are provided, and that the heap can be treated as if
it were an array (i.e. where indicies to successive elements are given by a
monotonically increasing, linear series of pointer offsets from the address of
the first element).

The underlying hardware must not be too picky about alignment. It should work on
x86 machines (32- and 64-bit).

C<my_bestfit_malloc> doesn't use a circular linked list, which means repeated
allocation of similarly sized amounts will likely result in quadratic behavior.
I may change this, but not until I've identified it as an actual bottleneck.

=cut
*/
void *my_bestfit_malloc(size_t size)
{
    if (!g_initialized)
        init_malloc();

    if (size == 0)
        return NULL;

    if (g_living_objects == 0) /* New allocation chain needed */
    {
        assert(g_head_ptr == NULL);
        assert(g_tail_ptr == NULL);
        void *base = sbrk_chk(size + sizeof(Node));
        assert(base == g_end);
        (void) base;
        return new_node(g_end, NULL, NULL, size);
    }

    /* Check if there's space at the base of the heap from a freed head */
    if (g_end != g_head_ptr)
    {
        ptrdiff_t region_size = (char *)g_head_ptr - (char *)g_end;
        assert(region_size > 0);
        if (region_size >= size + sizeof(Node))
            return new_node(g_end, NULL, g_head_ptr, size);
    }

    Node  *node, *best_prev, *best_next;
    void  *best_start;
    size_t best_fit = SIZE_MAX;
    size_t const required_size = size + sizeof(Node);
    for (node = g_head_ptr; node && node->next; node = node->next)
    {
        void     *free_ptr    = (char *)node + sizeof(node) + node->size;
        ptrdiff_t region_size = (char *)node->next - (char *)free_ptr;
        assert(region_size >= 0);   /* node.next should always reside in a
                                       higher memory location than node */
        if (region_size < required_size)
        {
            continue;
        }
        else if (region_size >= required_size)
        {
            if (region_size == required_size) /* Short circuit on perfect fit */
                return new_node(free_ptr, node, node->next, size);
            best_fit   = region_size;
            best_start = free_ptr;
            best_prev  = node;
            best_next  = node->next;
        }
    }

    if (best_fit == SIZE_MAX) /* Unchanged best_fit means no fit was found */
    {
        /* No existing nodes fit; this should be the case unless non-tail nodes
           have been freed */
        void *base = sbrk_chk(size + sizeof(Node));
        return new_node(base, g_tail_ptr, NULL, size);
    }
    else
    {
        return new_node(best_start, best_prev, best_next, size);
    }
    return NULL;
}

/**
=head2 static void *new_node(void *base, Node *prev, Node *next, size_t size)

Constructs a new Node at C<base>.

=cut
*/
static void *new_node(void *base, Node *prev, Node *next, size_t size)
{
    Node *new_node = base;
    new_node->prev = prev;
    new_node->next = next;
    new_node->size = size;
    if (next != NULL)
        next->prev = new_node;
    else
        g_tail_ptr = new_node;

    if (prev != NULL)
        prev->next = new_node;
    else
        g_head_ptr = new_node;

    ++g_alloc_count;
    ++g_living_objects;
    return (char *)base + sizeof(Node);
}

void my_free(void *ptr)
{
    if (ptr == NULL)
        return;
    Node *node = (Node *)((char *)ptr - sizeof(Node));
    if (node->next == NULL && node->prev == NULL)
    {
        /* Single node, is both head and tail */
        assert(g_living_objects == 1);
        brk_chk(g_end);
        g_head_ptr = NULL;
        g_tail_ptr = NULL;
    }
    else if (node->prev == NULL)
    {
        /* Head node */
        g_head_ptr = node->next;
        node->next->prev = NULL;
    }
    else if (node->next == NULL)
    {
        /* Tail node */
        node->prev->next = NULL;
        g_tail_ptr = node->prev;
        brk_chk((char *)g_tail_ptr + g_tail_ptr->size);
    }
    else
    {
        node->prev->next = node->next;
        node->next->prev = node->prev;
    }
    --g_living_objects;
    return;
}

#ifdef DEBUG
static void malloc_debug_exit(void)
{
    puts("========== MALLOC STATS ==========");
    printf(
        "Total number of allocations: %u\n"
        "Number of live allocations at exit: %u\n"
        "Start _brk: %p\n"
        "End   _brk: %p\n",
        g_alloc_count,
        g_living_objects,
        g_end,
        sbrk_chk(0)
    );
    dump_alloc_chain();
}

static void dump_alloc_chain(void)
{
    if (g_living_objects == 0)
        return;
    size_t i = 0;
    for (Node *node = g_head_ptr; node; node = node->next)
    {
        printf(
            "%zu NODE(%p):\n"
            "    prev: %p\n"
            "    next: %p\n"
            "    size: %zu\n",
            i,
            node,
            node->prev,
            node->next,
            node->size
        );
        ++i;
    }
}

static void assertion_exit(int signum)
{
    malloc_debug_exit();
}
#endif

static void init_malloc(void)
{
    #ifdef DEBUG
    atexit(malloc_debug_exit);

    struct sigaction sa;
    sa.sa_handler = assertion_exit;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGABRT, &sa, NULL) == -1)
    {
        perror(strerror(errno));
        exit(EXIT_FAILURE);
    }
    #endif
    g_end   = sbrk_chk(0);
    g_initialized = true;
}

/* Wrappers for sbrk() and brk() that check for errors */
static void *sbrk_chk(intptr_t increment)
{
    void *ret = sbrk(increment);
    if (ret == ((void *)-1))
    {
        perror(strerror(errno));
        exit(EXIT_FAILURE);
    }
    return ret;
}

static void brk_chk(void *addr)
{
    int ret = brk(addr);
    if (ret == -1)
    {
        perror(strerror(errno));
        exit(EXIT_FAILURE);
    }
}
