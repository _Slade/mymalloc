#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "mymalloc.h"

struct Node {
    int num;
    struct Node *next;
};

typedef struct {
    int length;
    struct Node *head;
    struct Node *tail;
} Linked_List;

bool LL_append(Linked_List *, int);
void LL_destroy(Linked_List *);
double LL_average(Linked_List *);

int main(int argc, char *argv[])
{
    int num;
    double average;
    Linked_List *list = my_bestfit_malloc(sizeof(Linked_List));
    for (num = -1; scanf("%d", &num); num = -1)
    {
        if (num == -1)
        {
            break;
        }
        LL_append(list, num);
    }

    average = LL_average(list);
    printf("Average: %f\n", average);

    LL_destroy(list);

    return EXIT_SUCCESS;
}

double LL_average(Linked_List *list)
{
    int augend = 0;
    struct Node *curnode;
    for (curnode = list->head; curnode; curnode = curnode->next)
    {
        augend += curnode->num;
    }
    return (double)augend / (double)list->length;
}

bool LL_append(Linked_List *list, int datum)
{
    if (!list)
        return false;

    struct Node *new_node = my_bestfit_malloc(sizeof(struct Node));
    if (!new_node) /* my_bestfit_malloc failure */
        return false;

    new_node->num = datum;

    if (list->head)
    {
        list->tail = list->tail->next = new_node;
    }
    else
    {
        list->head = list->tail = new_node;
    }

    ++list->length;
    return true;
}

void LL_destroy(Linked_List *list)
{
    struct Node *curnode = list->head;
    struct Node *this_node;
    while (curnode)
    {
        this_node = curnode;
        curnode = curnode->next;
        my_free(this_node);
    }
    my_free(list);
}
