CC           = gcc
INCLUDE_DIRS = -I.
OBJECTS      = $(addsuffix .o, mymalloc test1 mytap test2)
EXECUTABLES  = test1 test2
WARNINGS     = -Wall
CONF 		 = -O2 -UDEBUG -DNDEBUG
CFLAGS       = $(CONF) $(INCLUDE_DIRS) $(WARNINGS) -std=gnu99\
               -fno-strict-aliasing -fwrapv
EXE_RULE     = $(CC) -o $@ $^
define \n


endef

all : $(EXECUTABLES)

debug : CONF = -ggdb3 -DDEBUG
debug : SUFFIX = _debug
debug : all

test1 : test1.o mymalloc.o mytap.o
	$(EXE_RULE)

test2 : test2.o mymalloc.o
	$(EXE_RULE)

test1.o : test1.c mytap.h

test2.o : test2.c

mymalloc.o : mymalloc.c mymalloc.h

mytap.o : mytap.c mytap.h

clean:
	$(RM) $(OBJECTS) $(EXECUTABLES)

test :
	@echo Running tests...
	./test1
	./test2
	./mallocdrv | while read line; do if [[ "$$line" != +([0-9]) ]]; then echo "$$line"; fi; done

