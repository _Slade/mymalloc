#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

unsigned int test_number  = 0;  /* Current test_number */
unsigned int failed_tests = 0;

int diag(char const *fmt, ...)
{
    va_list arg;
    int rv;

    va_start(arg, fmt);
    rv = vfprintf(stderr, fmt, arg);
    va_end(arg);

    puts("");

    return rv;
}

void _ok1(_Bool cond, char const *file, unsigned int line)
{
    ++test_number;
    if (cond)
    {
        diag("ok %u", test_number);
    }
    else
    {
        diag(
            "not ok %u\n"
            "#   Failed test at %s line %u.",
            test_number, file, line
        );
        ++failed_tests;
    }
}

void _ok2(_Bool cond, char *msg, char const *file, unsigned int line)
{
    ++test_number;
    if (cond)
    {
        diag("ok %u - %s", test_number, msg);
    }
    else
    {
        diag(
            "not ok %u - %s\n# failed_tests test_number at %s line %u.",
            test_number, msg, file, line
        );
        ++failed_tests;
    }
}

void done_testing(void)
{
    diag("1..%u", test_number);
    if (failed_tests != 0)
    {
        diag(
            "# Looks like you failed %u test%s of %u",
            failed_tests,
            failed_tests == 1 ? "" : "s",
            test_number
        );
    }
    exit(EXIT_SUCCESS);
}
